﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PACMANMovement : MonoBehaviour
{
    [SerializeField]
    float MovementSpeed;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        //Debug.Log("h:" + h + " - v:" + v );

        if(h != 0)
        transform.Translate(Vector3.right * MovementSpeed * h);

        else
        transform.Translate(Vector3.up * MovementSpeed * v);
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("GNAM!"); 

        if(other.gameObject.tag == "pill")
        {
            Destroy(other.gameObject);
        }
    }

}
