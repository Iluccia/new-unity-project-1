﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoTest : MonoBehaviour {

    void Awake ()
    {
        Debug.Log("Awake");  
    }
   
    private void OnEnable()
    {
        Debug.Log("ENABLE");
    }

    private void OnDisable()
    {
        Debug.Log("DISABLE");
    }



    // Use this for initialization
    void Start () {
        Debug.Log("START");
	}
	


	// Update is called once per frame
	void Update () {
        Debug.Log("UPDATE");
	}

    private void OnPreRender()
    {
        Debug.Log("PRE RENDER");
    }


}