﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestClass : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        Dog a = new Dog(3);
        Dog b = new Dog(5);

        //Debug.Log("A is Sleeping" + a.IsSleeping());
        //a.Sleep();
        //Debug.Log("A is Sleeping" + a.IsSleeping());

        //Debug.Log("B is Sleeping" + b.IsSleeping());
        //Debug.Log("B is Sleeping" + b.IsSleeping());

        //Debug.Log("cane A ha" + a.GetAge() + "anni");
        //Debug.Log("cane B ha" + b.GetAge() + "anni");

        //Debug.Log("cane A ha" + a.GetAge() + "anni");
        //a.SetAge(5);
        //Debug.Log("cane A ha" + a.GetAge() + "anni");

        //a.Bark();

        //Debug.Log("dog speed" + a.speed);
        //a.Run(3.5f);
        //Debug.Log("dog speed" + a.speed);
    }


    // Update is called once per frame
    void Update () {
		
	}
}



class Dog
{
    int age;
    public float Speed;
    bool sleeping;

    public Dog()
    {

    }

    public Dog(int age)
    {
        this.age = age;
    }

    public void Bark()
    {
        Debug.Log("BAU!");

    }

    public void Run(float speed)
    {
        Speed = speed;
    }
    
    public void SetAge(int age)
    {
        this.age = age;

    }

    public int GetAge()
    {
        return this.age;
    }

    public void Sleep()
    {
       sleeping = true;
    }

    public bool IsSleeping()
    {
        return sleeping;

    }
}

