﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField]
    float Angle;

	// Use this for initialization
	void Start () {
        //Debug.Log(gameObject.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
        
        
        //Debug.Log(gameObject.transform.position);
        gameObject.transform.Rotate(Vector3.forward, Angle * Time.deltaTime);

    }
}
